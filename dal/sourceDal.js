const models = require('../models');
const prototypeDal = require('./prototypeDal');

module.exports = new prototypeDal.dal(models.source);