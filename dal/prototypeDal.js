const mongoose = require('mongoose');

module.exports.dal = function(model) {
    this.find = function(query, sortParameners, limitAmount, skipAmount) {
        return new Promise(function(resolve, reject) {
            model.find(query, null, {skip: skipAmount, limit: limitAmount, sort: sortParameners}, function(err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve(res.map(element => element.toObject()));
                }
            });
        });
    };

    return (this);
}