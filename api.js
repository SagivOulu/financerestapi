var express = require('express');
var router = express.Router();
var api_v1 = require('./api/v1');

router.use('/v1', api_v1);

router.get('/', function(req, res, next) {
  res.send("api root path");
});

module.exports = router;