require('dotenv').config();
const app = require('./app');
const http = require('http');
const fs = require('fs');
const normalizePort = require('normalize-port');

// Use env port, or 3000 if port not found 
var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

// Create an HTTP server
var server = http.createServer(app);
server.listen(port, function() {
    
});
