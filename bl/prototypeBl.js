module.exports = function bl(dal) {
    this.find = function(query, sortParameners, limitAmount, skipAmount) {
        return dal.find(query, sortParameners, limitAmount, skipAmount);
    };
    
    return (this);
}