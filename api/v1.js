var express = require('express');
var cryptoBl = require('../bl/cryptoBl');
var fiatBl = require('../bl/fiatBl');
var securityBl = require('../bl/securityBl');
var symbolSourceBl = require('../bl/symbolSourceBl');
var sourceBl = require('../bl/sourceBl');
const models = require('../models');
var router = express.Router();
var mongoose = require('mongoose');

router.get('/', function(req, res, next) {
  res.send("api v1 root");
});

const queryFilterOperators = [
    {public: 'lt', actual: '$lt'},
    {public: 'lte', actual: '$lte'},
    {public: 'gt', actual: '$gt'},
    {public: 'gte', actual: '$gte'},
    {public: 'in', actual: '$in'},
    {public: 'ne', actual: '$ne'},
    {public: 'nin', actual: '$nin'}
];
const querySortOperators = [
    {public: 'asc', actual: 1},
    {public: 'dsc', actual: -1}
];
function precisionRound(number, precision) {
    var factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
}

var buildQuery = function(reqQuery, model) {
    var query = {filter: {}, sort: {}, page: 0, per_page: 0};
    // For each parameter passed in the input query
    for (var property in reqQuery) {

        // If it is a parameter in the model, add it to the query filter
        if ((reqQuery.hasOwnProperty(property)) && 
            (model.schema.obj.hasOwnProperty(property))) {

            query.filter[property] = reqQuery[property];

        } else if ((property == 'page') || (property == 'per_page')) {
            query[property] = reqQuery[property];
        } else {

            // Check if the parameter matches a filter operator
            for (let operator of queryFilterOperators) {
                var regxp = new RegExp('_' + operator.public + '$');
                if (property.match(regxp)) {
                    var propWithoutOperator = property.replace(regxp, '');

                    // Check if the property without the operator matches a property in the model
                    if (model.schema.obj.hasOwnProperty(propWithoutOperator)) {
                        
                        // If the query already contains the parameter, add the filter to the parameter
                        if (query.filter.hasOwnProperty(propWithoutOperator)) {
                            query.filter[propWithoutOperator][operator.actual] = reqQuery[property]
                        } else {
                            var finalValue = reqQuery[property];

                            // Convert the input array to a format usable by mongoose. for example:
                            // [BTC,DOGE] -> ["BTC","DOGE"]
                            var arrayRegexp = new RegExp(/^\[([^,]*,)*([^,]*)?\]$/);
                            if (reqQuery[property].match(arrayRegexp)) {
                                finalValue = finalValue.replace(/\[/, "[\"");
                                finalValue = finalValue.replace(/\]/, "\"]");
                                finalValue = finalValue.replace(/,/, "\",\"");
                            }

                            // Add the property and the operator to the query filter
                            var jsonString = ""
                            if ((operator.public == 'in') || (operator.public == 'nin')) {
                                jsonString = '{"' + operator.actual + '":' + finalValue + '}';
                            } else {
                                jsonString = '{"' + operator.actual + '":"' + finalValue + '"}';
                            }
                            query.filter[propWithoutOperator] = JSON.parse(jsonString);
                        }
                    }
                }
            }

            // Check if the parameter matches a sort operator
            for (let operator of querySortOperators) {
                var regxp = new RegExp('_' + operator.public + '$');
                if (property.match(regxp)) {
                    var propWithoutOperator = property.replace(regxp, '');

                    // Check if the property without the operator matches a property in the model
                    if (model.schema.obj.hasOwnProperty(propWithoutOperator)) {
                        
                        // Add the property and the operator to the query sort
                        query.sort[propWithoutOperator] = operator.actual;
                    }
                }
            }
        }
    }

    return (query);
}

router.get('/crypto', function(req, res) {
    var builtQuery = buildQuery(req.query, models.crypto);
    cryptoBl.find(builtQuery.filter, builtQuery.sort, builtQuery.per_page, builtQuery.page * builtQuery.per_page)
        .then(async function(cryptos) {

            // Search for additional properties in the url query
            for (var property in req.query) {
                if (property == 'currency') {

                    var arrStrCurrencies = req.query[property].replace(/(^\[)|(\]$)/, '').split(',');

                    // For each symbol, add it as a currency to the result
                    for (let strCurrency of arrStrCurrencies) {

                        // find matching crypto
                        var arrCurrencyCryptos = null;
                        await cryptoBl.find({symbol: strCurrency})
                            .then(function(cryptos) {
                                if ((cryptos != null) && (cryptos.length > 0)) {
                                    arrCurrencyCryptos = cryptos;
                                }
                            })
                            .catch(function(err) {
                            
                            });
                        
                        // if matching currency cryptos found, calc the price for each of them
                        if (arrCurrencyCryptos != null) {
                        
                            // find the currency crypto matching each date in the cryptos array, and add the price accordingly
                            cryptos = cryptos.map(crypto => {
                                var matchingCurrencyCrypto = arrCurrencyCryptos.find(currencyCrypto => currencyCrypto.date == crypto.date);
                            
                                // If match found, calc the price and add it. else, set the value to an error message
                                if (matchingCurrencyCrypto) {
                                    crypto['price_' + strCurrency] = precisionRound(crypto.price_usd / matchingCurrencyCrypto.price_usd, 4);
                                } else {
                                    crypto['price_' + strCurrency] = 'ERROR: Missing convertion data for the given date';
                                }
                            
                                return (crypto);
                            });
                        
                        } else {

                            // if no matching crypto, find matching fiat
                            var arrCurrencyFiats = null;
                            var arrInverseCurrencyFiats = null;
                            await fiatBl.find({from_currency: 'USD', to_currency: strCurrency})
                                .then(function(fiats) {
                                    if ((fiats != null) && (fiats.length > 0)) {
                                        arrCurrencyFiats = fiats;
                                    }
                                })
                                .catch(function(err) {
                                    console.log('error = ' + err);
                                });
                            await fiatBl.find({from_currency: strCurrency, to_currency: 'USD'})
                                .then(function(fiats) {
                                    if ((fiats != null) && (fiats.length > 0)) {
                                        arrInverseCurrencyFiats = fiats;
                                    }
                                })
                                .catch(function(err) {
                                    console.log('error = ' + err);
                                });
                            
                            // if matching currency cryptos found, calc the price for each of them
                            if ((arrCurrencyFiats != null) || (arrInverseCurrencyFiats != null) || (strCurrency == 'USD')) {
                            
                                // find the currency fiat matching each date in the fiats array, and add the price accordingly
                                cryptos = cryptos.map(crypto => {

                                    if (strCurrency == 'USD') {
                                        crypto['price_' + strCurrency] = crypto.price_usd;
                                    } else {
                                        var matchingCurrencyFiat = (arrCurrencyFiats == null ? null : arrCurrencyFiats.find(currencyFiat => currencyFiat.date == crypto.date));
                                        var inverseMatchingCurrencyFiat = (arrInverseCurrencyFiats == null ? null : arrInverseCurrencyFiats.find(currencyFiat => currencyFiat.date == crypto.date));
                                        
                                        if (matchingCurrencyFiat) {
                                            crypto['price_' + strCurrency] = precisionRound(crypto.price_usd * matchingCurrencyFiat.factor, 4);
                                        } else if (inverseMatchingCurrencyFiat) {
                                            crypto['price_' + strCurrency] = precisionRound(crypto.price_usd / inverseMatchingCurrencyFiat.factor, 4);
                                        } else {
                                            crypto['price_' + strCurrency] = 'ERROR: Missing convertion data for the given date';
                                        }
                                    }
                                
                                    return (crypto);
                                });
                            } else {
                            
                                // no matching crypto or fiat found, set value to 'no matching symbol found'
                                cryptos = cryptos.map(crypto => {
                                    crypto['price_' + strCurrency] = 'ERROR: no matching symbol found';
                                    return crypto;
                                });
                            }
                        }
                    }
                }
            }

            res.send(cryptos);
        })
        .catch(function(err) {
            console.error(err);
            throw err;
        });
});

router.get('/fiat', function(req, res) {
    var builtQuery = buildQuery(req.query, models.fiat);
    fiatBl.find(builtQuery.filter, builtQuery.sort, builtQuery.per_page, builtQuery.page * builtQuery.per_page)
        .then(function(fiats) {
            res.send(fiats);
        })
        .catch(function(err) {
            console.error(err);
            throw err;
        });
});

router.get('/security', function(req, res) {
    var builtQuery = buildQuery(req.query, models.security);
    securityBl.find(builtQuery.filter, builtQuery.sort, builtQuery.per_page, builtQuery.page * builtQuery.per_page)
        .then(function(securities) {
            res.send(securities);
        })
        .catch(function(err) {
            console.error(err);
            throw err;
        });
});

router.get('/symbolSource', function(req, res) {
    var builtQuery = buildQuery(req.query, models.symbolSource);
    symbolSourceBl.find(builtQuery.filter, builtQuery.sort, builtQuery.per_page, builtQuery.page * builtQuery.per_page)
        .then(function(symbolSources) {
            res.send(symbolSources);
        })
        .catch(function(err) {
            console.error(err);
            throw err;
        });
});

router.post('/symbolSource', function(req, res) {
    console.log(req.body);
    res.send('hi there');
    // symbolSourceBl.find(buildQuery(req.query, models.symbolSource), function(err, data) {
    //     res.send(data);
    // });
});

router.get('/source', function(req, res) {
    var builtQuery = buildQuery(req.query, models.source);
    sourceBl.find(builtQuery.filter, builtQuery.sort, builtQuery.per_page, builtQuery.page * builtQuery.per_page)
        .then(function(sources) {
            res.send(sources);
        })
        .catch(function(err) {
            console.error(err);
            throw err;
        });
});


module.exports = router;
