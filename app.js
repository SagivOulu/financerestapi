const express = require('express');
const mongoose = require('mongoose');
const api = require('./api');
const models = require('./models');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));

var dbconstr = 'mongodb://' + 
        (process.env.DBIP || '192.168.1.179') + ':' + 
        (process.env.DPPORT || '60311') + '/' + 
        (process.env.DBNAME || 'symbols');
mongoose.connect(dbconstr);

var db = mongoose.connection;
db.on('error', function() {
    console.error('error while connecting to mongodb');
});
db.on('open', function() {
    console.log('connection to db succesful!');
});

// Insert routing here
app.use('/api', api);

// 404 not found error handeling
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    res.status(404).send('404 Not Found');
});

module.exports = app;