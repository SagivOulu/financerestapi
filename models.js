const mongoose = require('mongoose');

const cryptoCollection = (process.env.CRYPTOCOLLECTION || 'crypto');
const fiatCollection = (process.env.FIATCOLLECTION || 'fiat');
const securityCollection = (process.env.SECURITYCOLLECTION || 'security');
const symbolSourceCollection = (process.env.SYMBOLSOURCECOLLECTION || 'SymbolSources');
const sourceCollection = (process.env.SOURCECOLLECTION || 'Sources');
var cryptoSchema = mongoose.Schema(
{
	"24h_volume_usd" : {type: String},
	"available_supply" : {type: String},
	"date" : {type: String},
	"market_cap_usd" : {type: String},
	"percent_change_1m" : {type: String},
	"percent_change_1y" : {type: String},
	"percent_change_24h" : {type: String},
	"percent_change_3m" : {type: String},
	"percent_change_7d" : {type: String},
	"percent_change_ytd" : {type: String},
	"price_usd" : {type: String},
	"rank" : {type: Number},
	"symbol" : {type: String},
	"updated" : {type: Date}
});

module.exports.crypto = mongoose.model(cryptoCollection, cryptoSchema, cryptoCollection);

var fiatSchema = mongoose.Schema({
    "to_currency" : {type: String},
    "date" : {type: String},
    "from_currency" : {type: String},
    "factor" : {type: Number}
});

module.exports.fiat = mongoose.model(fiatCollection, fiatSchema, fiatCollection);

var securitySchema = mongoose.Schema({
    "date" : {type: String},
    "symbol" : {type: String},
    "currency" : {type: String},
    "price" : {type: Number}
});

module.exports.security = mongoose.model(securityCollection, securitySchema, securityCollection);

var symbolSourceSchema = mongoose.Schema({
    "source" : {type: String},
    "symbol" : {type: String},
    "type" : {type: String}
});

module.exports.symbolSource = mongoose.model(symbolSourceCollection, symbolSourceSchema, symbolSourceCollection);

var sourceSchema = mongoose.Schema({
    "source" : {type: String},
    "date" : {type: String},
    "asof" : {type: String},
    "time" : {type: String}
});

module.exports.source = mongoose.model(sourceCollection, sourceSchema, sourceCollection);
